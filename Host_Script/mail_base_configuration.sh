#!/bin/bash -
#===============================================================================
#
#          FILE: mail_vm_create_test.sh
#
#         USAGE: ./vm_create_test.sh
#
#   DESCRIPTION: creates vm automatically without user input
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: John Choi, yno@my.bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/10/2017 12:26
#      REVISION:  ---
#===============================================================================

set -o nounset                        # Treat unset variables as an error

declare VM
declare vm_HDPath
declare delResponse="n"
declare vm_createExit
declare delete_Exit


VM="mail"
vm_HDPath=""

function mailvm_createcf() {
echo =====================================================================
echo Creating VM
echo =====================================================================

sleep 1

#Create VM
vboxmanage createvm --name $VM --ostype "RedHat_64" --register

vm_HDPath=$( vboxmanage showvminfo s3rtr | grep "Config file:" | sed -e 's/Config file:\s*//' -e 's/\(\/.*\/\).*/\1/')
# Create Medium
vboxmanage createmedium --filename "${vm_HDPath}/$VM" --size 10000
sleep 1
}
vm_createExit=$?


function mailvm_storagecf() {
echo =====================================================================

echo Add SATA controller

# Add SATA controller
vboxmanage storagectl $VM --name "SATA Controller" --add sata --controller IntelAhci
vboxmanage storageattach $VM --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "${vm_HDPath}/$VM.vdi"
sleep 1

}

function mailvm_idecf() {
echo =====================================================================

echo ADD IDE controllers
declare iso_address

# Add an IDE controller with a DVD drive attached and add iso file into the drive.
vboxmanage storagectl $VM --name "IDE Controller" --add ide
vboxmanage storageattach $VM --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium ~/Downloads/autorun.iso
sleep 1
}

function mailvm_apiccf() {

echo =====================================================================
echo Enable/Disable Input and Output APIC

# Enable/Disable Input and Output APIC
vboxmanage modifyvm $VM --ioapic on
sleep 1
}

function mailvm_bootcf() {

echo =====================================================================
echo Configuring Boot Order

# Configure boot order
vboxmanage modifyvm $VM --boot1 disk --boot2 dvd --boot3 none --boot4 none
sleep 1
}

function mailvm_ramcf() {

echo =====================================================================
echo Configuring RAM Settings
# RAM and vRAM settings
vboxmanage modifyvm $VM --memory 512 --vram 128
sleep 1
}

function mailvm_netcf() {

echo =====================================================================

echo Setting up Network Settings
# Network configuration
#vboxmanage modifyvm $VM --nic1 bridged --bridgeadapter1 VLAN2016 --nicpromisc1 allow-vms --nictype1 virtio
vboxmanage modifyvm $VM --nic1 intnet --intnet1 as_learn --nicpromisc1 allow-vms --nictype1 virtio
vboxmanage modifyvm $VM --macaddress1 auto
#vboxmanage modifyvm $VM --macaddress2 auto
sleep 1
}

function mailvm_usbcf() {

echo =====================================================================
echo Enabling USB
vboxmanage modifyvm $VM --usb on
sleep 1
}

function mailvm_startcf() {

echo =====================================================================
echo VM $VM installation completed

sleep 1

echo Starting the Virtual Machine $VM

vboxmanage startvm $VM

}

#=========================================================================

function deleteStuff() {
                declare delResponse="n"
                        echo "path is ${vm_HDPath}"
                        read -p "Wanna delete it all?: " delResponse
                        echo $delResponse
                        if [[ ${delResponse} == "y" ]]
                        then
                                echo "received y"
                                #delet stuff
                #               VboxManage 'unregistervm "${vm_mail}" —delete'
                                vboxmanage unregistervm ${VM} --delete
                                rm -rf "${vm_HDPath}"
                        fi
                        delete_Exit=$?
}







#deleteStuff

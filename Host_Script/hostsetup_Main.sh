#!/bin/bash -
#===============================================================================
#
#          FILE: main.sh
#
#         USAGE: ./main.sh
#
#   DESCRIPTION: Main invocation script
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: John Choi, yno@my.bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/30/2017 12:16
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare returncode=1
#===============================================================================
#base_configuration.sh: Configure Basic System
#automated installation of virtual machine
#SSH access to VM

source script_funcFile.sh
##Download iso file from the repository autorun.iso file (700MB)
iso_repoDown
sleep 1

##Checking to see if certain applications are installed on host machine
app_installCheck expect
sleep 1

#Automated VM creation with configuration

source base_configuration.sh
vm_createcf
vm_storagecf
vm_idecf
vm_apiccf
vm_bootcf
vm_ramcf
vm_netcf
vm_usbcf
vm_startcf

source mail_base_configuration.sh
mailvm_createcf
mailvm_storagecf
mailvm_idecf
mailvm_apiccf
mailvm_bootcf
mailvm_ramcf
mailvm_netcf
mailvm_usbcf
mailvm_startcf

echo "Now this will take some time so grab a coffee or comeback in about ~7 minutes"
sleep 450

rsa_keyFind
scp_clientMachine
ssh-prepKey

source git_conHost.#!/bin/sh
git_connect

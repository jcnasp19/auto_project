#!bin/bash

function app_installCheck {
	declare app
	declare app_install
	declare app_exit
	declare -ri NUMARGS=$#

echo "========================================================================="
echo "Checking for installed applications"
	app=$@
	locate $@ > /tmp/locate_excess
  locate_appInstall=$?

if [[ $locate_appInstall == 0 ]]; then
	            echo "$@ already installed"

else
	             for app in "$@"
	             do
		                sudo yum install -y $app
	             done
fi

app_exit=$?
if [[ $app_exit == 1 ]]; then
	echo "Please install "$app" manually"
fi
}

declare key_copyExit

function rsa_keyFind() {

  declare find_keyFile
  declare public_keyFile
  declare public_hasKey
  declare host_name
  declare secure_cp

#update locate database table
#sudo updatedb

#locate id_rsa.pub file on host system
find_keyFile=$( locate id_rsa.pub )
public_keyFile=$?

#checking to see if file has contents
grep -q ssh-rsa "$find_keyFile"
file_hasKey=$?

#assign a variable to locate test with exit code
if [[ $public_keyFile != 0 ]]; then

	#ssh keygen program and saving it to directory
	ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa.pub -q -P ""
else

  #File was found, check if there was a key
	echo "Key file found and checking for the key"
fi

if [[ $file_hasKey == 0 ]]; then

		echo "ssh-rsa key found in the file"
		#We have a key so we can proceed
else 	#Key not found
		echo "No ssh public key found in $find_keyFile"
		ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa.pub -q -P ""
fi

#Make a key and append to the file
cat "$find_keyFile" | grep ssh-rsa > /tmp/public_key.pub

#exit code for echo "copy file"
key_copyExit=$?

}

function scp_clientMachine() {
##Creating a secret copy to VM Client machine
#echo host_nameQ
if [[ $key_copyExit == 0 ]]; then

      expect -c "
	     set timeout 1
	      spawn scp /tmp/public_key.pub sysadmin@10.16.255.3:/home/sysadmin/.ssh/authorized_keys
	       expect yes/no { send yes\r ; exp_continue }
	        expect password: { send password\r }
	         expect 100%
	          sleep 1
	           exit
             "
fi

secure_cp=$?
if [[ $secure_cp == 0 ]]; then
		rm -rf /tmp/public_key.pub
fi

}

function iso_repoDown() {

declare iso_Down
#declare iso_Location
echo "========================================================================="
echo "Downloading iso file from online repository"

wget https://nasp.htp.bcit.ca/files/kickstart/autorun.iso --user nasp19 --password student -O ~/Downloads/autorun.iso
iso_Down=$?

#if [[ $iso_Down == 0 ]]; then
#  iso_Location=$( locate autorun.iso )
#fi
}

function selinux_disable() {
echo "========================================================================="
echo "Disabling selinux"
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config
}

function clear_knownHosts() {

sed -i 's/10.16.255.3//g' ~/.ssh/known_hosts

}

function ssh-prepKey() {
echo "========================================================================="
echo "Disabling ssh password prompt"
#echo "sysadmin ALL=(ALL) NOPASSWD: ALL ">> /etc/sudoers
sudo -i sed -i -e 's/#PermitRootLogin yes/PermitRootLogin without-password/g' /etc/ssh/sshd_config
sudo -i sed -i -e 's/#StrictModes yes/StrictModes no/g' /etc/ssh/sshd_config
sudo -i sed '49,50!d' /etc/ssh/sshd_config
sudo -i sed -i -e 's/Defaults     env_reset/Defaults     env_reset,timestamp_timeout=30/g' /etc/sudoers

sudo systemctl restart sshd
}


function git_connect() {
  #statements

	ssh sysadmin@10.16.255.3 'bash -c "sudo yum install -y git"'
	ssh sysadmin@10.16.255.3 'bash -c "sudo chown -R sysadmin:sysadmin ./Scripts"'

	expect -c "
	spawn scp -r ./git_initVM.sh sysadmin@10.16.255.3:./Scripts
	  set timeout 5
	  expect password: { send password\r }
	  expect 100%
	  sleep 1
	"
	ssh sysadmin@10.16.255.3 'bash -c "bash /home/sysadmin/Scripts/git_initVM.sh"'

}

#!/bin/bash -
#===============================================================================
#
#          FILE: basic_file_test.sh
#
#         USAGE: ./basic_file_test.sh
#
#   DESCRIPTION: Receives a single argument and the argument gets stored in a local variable. 
#		 The single arguments will undergo 'if' testing and return an exit code of 0 or 1. 
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: John Choi, yno@my.bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/10/2017 12:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Variable declarations
#todo place variable declarations here

declare -r SCRIPTVERSION="1.0"
declare file_name
declare whereis_output
declare install_command
declare locations

#declare -i package_installed=1

#Solicit command that should be searched and store it in the varialbe command
#todo: use read -p statement to get input from user and store it in the variable: cmd

#read -p "Please enter the command you're looking for:" command_name

#Using user input as an argument

file_name=$1 

#Store the output of the whereis invocation that searches for the users inputed
# command in the variable "whereis_output"
#todo variable assigment to output of command - follows form variable=$( command )

whereis_output=$( locate $file_name 2> /dev/null )

#Using the variable whereis_output process the output to drop the command name
whereis_output=$?

if [[$file_name == ""]]; then
	echo "Please enter a fully qualified file name"

elif [[ $whereis_output == 0 ]] ; then
  	echo "$file_name exists" 

else
  	echo "$file_name can't be found"
fi

echo "Exit code of this function is: $whereis_output"

exit $whereis_output




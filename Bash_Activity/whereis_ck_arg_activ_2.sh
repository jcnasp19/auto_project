#!/bin/bash -
#===============================================================================
#
#          FILE: whereis_check.sh
#
#         USAGE: ./whereis_check.sh
#
#   DESCRIPTION: Outputs the location of a command binary and returns an exit code
#                of zero if it is avaible or 127 if it can't be found
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: John Choi, yno@my.bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/10/2017 12:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Variable declarations
#todo place variable declarations here

declare -r SCRIPTVERSION="1.0"
declare command_name
declare whereis_output
declare install_command
#declare -i package_installed=1

#Solicit command that should be searched and store it in the varialbe command
#todo: use read -p statement to get input from user and store it in the variable: cmd

#read -p "Please enter the command you're looking for:" command_name

#Using user input as an argument

command_name=$1 

#Store the output of the whereis invocation that searches for the users inputed
# command in the variable "whereis_output"
#todo variable assigment to output of command - follows form variable=$( command )

whereis_output=$( whereis $command_name 2> /dev/null )

#Using the variable whereis_output process the output to drop the command name
#whereis_output=$?

#store the output in locations
locations=$( (echo $whereis_output | cut -d ":" -f 2-) )

if [[ $locations == "" ]];

then echo "The command is not installed on the computer."
 
  #the locations are empty i.e. command not found, tell the user
  #and set the exit code

else
  echo "Located at $whereis_output " 

#  echo "Located at"
#  echo $whereis_output
  #the locations contains the path to the command tell the user
  #and set the exit code
fi



#!/bin/bash - 
#===============================================================================
#
#          FILE: echo_main.sh
# 
#         USAGE: ./echo_main.sh 
# 
#   DESCRIPTION: This demonstates the ability of one script to include others
# 
#       OPTIONS: ---
#  REQUIREMENTS: echo_argument.sh and echo_argument2.sh must live in the same
#                directory.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: John Choi
#  ORGANIZATION: BCIT
#       CREATED: 04/19/2017 13:31
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare script_name=$0
echo -e "\nBeginning $script_name"

source ./echo_argument.sh
. ./echo_argument2.sh

echo -e "\nEnding $script_name\n"

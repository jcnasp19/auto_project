#!/bin/bash -
#===============================================================================
#
#          FILE: whereis_check.sh
#
#         USAGE: ./whereis_check.sh
#
#   DESCRIPTION: Outputs the location of a command binary and returns an exit code
#                of zero if it is avaible or 127 if it can't be found
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: John Choi, yno@my.bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/10/2017 12:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Variable declarations
#todo place variable declarations here

declare -r SCRIPTVERSION="1.0"
declare command_name
declare whereis_output
declare install_command
declare -i package_installed=1
declare success_install 
declare install_package

#Solicit command that should be searched and store it in the varialbe command
#todo: use read -p statement to get input from user and store it in the variable: cmd

read -p "Please enter the command you're looking for:" command_name

#Store the output of the whereis invocation that searches for the users inputed
# command in the variable "whereis_output"
#todo variable assigment to output of command - follows form variable=$( command )

whereis_output=$( whereis $command_name 2> /dev/null )

#Using the variable whereis_output process the output to drop the command name
#whereis_output=$?

#store the output in locations
locations=$( (echo $whereis_output | cut -d ":" -f 2-) )

if [[ $locations == "" ]];

then 
	read -p "The command is not installed on the \
	computer. Proceed with installation (y/n): " response 

	if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]];

	then
		install_package=1
	else
		install_package=0
	fi
fi

if [[ $install_package == 1 ]]; then
	
	success_install=$( sudo yum install $command_name 2> /dev/null )
	success_install=$?

	if [[ "success_install" == "0" ]];

	then 
		echo "Successfully installed";

	elif [[ "success_install" == "1" ]]
		then
		echo "Failed to install"
	fi
fi

if [[ $install_package == 0]]; then

	echo "Please attempt to install the command at a later date"

fi





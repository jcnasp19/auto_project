#!/bin/bash -
#===============================================================================
#
#          FILE: func_file_test.sh
#
#         USAGE: ./func_file_test.sh
#
#   DESCRIPTION: basic file existence test contained in a function. 
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: John Choi, yno@my.bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/19/2017 12:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare NUMARGS=$#

source ./function_source.sh 

function_entry_source 





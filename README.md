# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This script will allow complete automation of CentOS 7 installation. 
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
- Two folders are available on the repository. The folders are labelled as "Host_Scripts" and "VM_Scripts".

1. "HOSTSETUP_MAIN.SH"
	
	This file should be run first which would allow automatic creation of Virtual Machines as well as some 		of the basic configurations.

2. "VMSETUP_MAIN.SH"

	Once hostsetup_main.sh script is finalized then vmsetup_main.sh script should be executed for Virtual 		Machine setup.
 
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

If you have any questions please contact the author, John Choi @email: yno@my.bcit.ca. 
* Other community or team contact

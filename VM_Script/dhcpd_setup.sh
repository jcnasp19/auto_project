#!bin/bash

##dhcpd installation


function dhcp_intSetup () {
echo "========================================================================="
echo "Setting dhcp interface"

cp /usr/lib/systemd/system/dhcpd.service /etc/systemd/system/dhcpd.service

nmcli dev status | grep wifi > /tmp/wifi_name.txt
iw_devInt=$( sed 's/|/ /' /tmp/wifi_name.txt | awk '{print $1}'

cat << _EOF > /etc/dhcp/dhcpd.conf

[Unit]
Description=DHCPv4 Server Daemon
Documentation=man:dhcpd(8) man:dhcpd.conf(5)
Wants=network-online.target
After=network-online.target
After=time-sync.target

[Service]
Type=notify
ExecStart=/usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid eth1 adapter

[Install]
WantedBy=multi-user.target

_EOF

sed -i "s/adapter/$iw_devInt/" /etc/dhcp/dhcpd.conf
  
}

function dhcp_confSetup () {
echo "========================================================================="
echo "Setting dhcp configuration file"

cat << EOF > /etc/dhcp/dhcpd.conf

#Wired Connections
	subnet 10.16.3.126 netmask 255.255.255.240 {

	option routers 10.16.3.126;
	range 10.16.3.130 10.16.3.144
	}

#Wifi Network
	subnet 10.16.3.128 netmask 255.255.255.240 {

	option routers 10.16.3.128
	range 10.16.3.200 10.16.3.214
	}

EOF

systemctl restart dhcpd
systemctl enable dhcpd

}

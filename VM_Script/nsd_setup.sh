#!bin/bash

function nsd_appSetup() {

echo "========================================================================="
echo "Checking to see if nsd was installed on this machine"

source script_funcFile.sh
app_installCheck nsd

}

function nsd_confFile() {

cat << EOF >> /etc/nsd/nsd.conf

server:
	ip-address: 10.16.255.3
	do-ip4: yes
	do-ip6: no
	port: 53
	database: ""
	include: "/etc/nsd/server.d/*.conf"
	include: "/etc/nsd/conf.d/*.conf"
remote-control:
	control-enable: yes
zone:
	name: "s03.as.learn"
	zonefile: "s03.as.learn.zone"

zone:
	name: "3.16.10.in-addr.arpa"
	zonefile: "3.16.10.in-addr.arpa.zone"

EOF

systemctl start nsd
systemctl enable nsd

}

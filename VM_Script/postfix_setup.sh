#!bin/bash

function postfix_appSetup() {
echo "========================================================================="
echo "Checking to see if postfix is installed on the machine"
  source script_funcFile
  app_installCheck postfix

}

function postfix_confSetup() {
echo "========================================================================="
echo "Postfix configuration file setup"

sed -i '116s/localhost/all/' /etc/postfix/main.cf 
sed -i '164s/\(.*\)/\1, $mydomain/' /etc/postfix/main.cf
sed -i '419s/\#/\ /g' /etc/postfix/main.cf 

systemctl restart postfix.service

}

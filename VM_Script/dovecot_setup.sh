#!bin/bash

function dovecot_appSetup() {
echo "========================================================================="
echo "Checking to see if dovecot is installed on the machine"
  source script_funcFile.sh
  app_installCheck dovecot

}

function dovecot_confSetup() {
echo "========================================================================="
echo "Dovecot configuration file setup"

systemctl enable dovecot

sed -i '24s/#protocols = imap pop3 lmtp/protocols = imap lmtp/' /etc/dovecot/dovecot.conf
sed -i '24s/.//' /etc/dovecot/conf.d/10-mail.conf
sed -i '10s/.//' /etc/dovecot/conf.d/10-auth.conf
sed -i '10s/yes/no/' /etc/dovecot/conf.d/10-auth.conf

systemctl restart dovecot

}

#!bin/bash

#===============================================================================
#Allow root privileges

source script_funcFile.sh
root_login

exit

#===============================================================================
#selinux_setup.sh: Configure i.e. disable SELINUX
source selinux_setup.sh
source script_funcFile.sh

selinux_disable
sleep 1
#===============================================================================
#dhcp_setup.sh: Configure dhcpd service
source dhcpd_setup.sh
app_installCheck dhcpd
dhcp_intSetup
dhcp_confSetup

#===============================================================================
#network_setup.sh: Configure networking on the system and possibly also disable firewalld
# 		   and disable NetworkManager if they are not used. Should invoke or source
#		   subscripts:
#		   	net_if_setup.sh
#			wifi_if_setup.sh

source network_setup.sh
net_ospfSetup
ospf_zebraConf
ospf_zebraFunc
ospf_ospfConf

#===============================================================================
#iptables_setup.sh: Configure any iptables rules

#source iptables_setup.sh

#===============================================================================
#nsd_setup.sh: configure authoritative name service

source nsd_setup.sh
nsd_appSetup
nsd_confFile

#===============================================================================
#unbound_setup.sh: Configure recursive name service

source unbound_setup.sh
unbound_appSetup
unbound_rootHints
unbound_confSetup

#===============================================================================
#hostapd_setup.sh: Configure hostapd service

source net_iwSetup.sh
app_installCheck hostapd
iw_devName
iw_confFile
iw_apdSetup

#===============================================================================
#postfix_setup.sh: Configure mail transfer agent

source postfix_setup.sh
postfix_appSetup
postfix_confSetup

#===============================================================================
#dovecot_setup.sh: Configure mail delivery agent

source dovecot_setup.sh
dovecot_appSetup
dovecot_confSetup
#===============================================================================

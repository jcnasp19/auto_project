#!bin/bash

function net_ospfSetup() {

declare quagga_appInstall
declare selinux_appOff
declare quagga_Version

selinux_appOff=$( cat /etc/selinux/config | grep SELINUX=permissive )
selinux_appOff=$?

if  [[ $selinux_appOff == 1 ]]; then
        sed -i -e 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
else
        source script_funcFile.sh
        selinux_disable
fi

chown quagga:quagga /etc/quagga
}

function ospf_zebraConf() {

cat << EOT >> /etc/quagga/zebra.conf

!
! Zebra configuration saved from vty
!   2017/06/25 19:05:03
!
hostname Router
password zebra
enable password zebra
log file /var/log/quagga/quagga.log
!
interface eth1
 description internal_network
 ip address 10.16.3.126/25
 ipv6 nd suppress-ra
!
interface eth0
 description vlan2016
 ip address 10.16.255.3/25
 ipv6 nd suppress-ra
!
interface lo
!
ip forwarding
!
!
line vty
!

EOT

systemctl start zebra
systemctl enable zebra

}

function ospf_ospfConf() {

cat << _EOF >>  /etc/quagga/ospfd.conf

!
! Zebra configuration saved from vty
!   2017/04/25 19:05:03
!
hostname ospfd
password zebra
log stdout
!
!
!
interface eth1
!
interface eth0
!
interface lo
!
router ospf
 ospf router-id 10.16.255.3
 network 10.16.3.126/25 area 0.0.0.0
 network 10.16.255.3/25 area 0.0.0.0
!
line vty
!

_EOF

systemctl start ospfd
systemctl enable ospfd
}

sleep 2


function ospf_zebraFunc() {
echo "========================================================================="
echo "Checking to see if zebra is working"
declare zebra_confCheck

zebra_confCheck=$( zebra -Cf /etc/quagga/zebra.conf )

if  [[ $zebra_confCheck == 1 ]]; then

cat /etc/quagga/ospfd.conf

fi

}

#!bin/bash

function unbound_appSetup () {

echo "========================================================================="
echo "Checking to see if unbound was installed on this machine"
source script_funcFile
app_installCheck unbound
}

function unbound_rootHints () {

sudo wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints

}

function unbound_confSetup () {

cat << EOF >> /etc/unbound/unbound.conf

  verbosity: 1
	statistics-interval: 0
	statistics-cumulative: no
	extended-statistics: yes
	num-threads: 2
	interface: 10.16.3.126
	interface: 127.0.0.1
	interface-automatic: no
	port: 53
	num-queries-per-thread: 8314
	do-ip4: yes
	do-ip6: no
	do-udp: yes
	do-tcp: yes
	access-control: 0.0.0.0/0 refuse
	access-control: 127.0.0.0/8 allow
	access-control: ::0/0 refuse
	access-control: 10.16.3.0/25 allow
	access-control: 10.16.3.128/25 allow
	chroot: ""
	username: "unbound"
	directory: "/etc/unbound"
	log-time-ascii: yes
	pidfile: "/var/run/unbound/unbound.pid"
	harden-glue: yes
	harden-dnssec-stripped: yes
	harden-below-nxdomain: yes
	harden-referral-path: yes
	use-caps-for-id: no
	private-domain: "as.learn"
	private-domain: "htpbcit.ca"
        local-zone: "10.in-addr.arpa." nodefault
        local-zone: "16.10.in-addr.arpa." nodefault
	unwanted-reply-threshold: 10000000
	prefetch: yes
	prefetch-key: yes
	rrset-roundrobin: yes
	minimal-responses: yes
	module-config: "iterator"
	trusted-keys-file: /etc/unbound/keys.d/*.key
	auto-trust-anchor-file: "/var/lib/unbound/root.key"
	val-clean-additional: yes
	val-permissive-mode: no
	val-log-level: 1
	include: /etc/unbound/local.d/*.conf
remote-control:
	control-enable: yes
	server-key-file: "/etc/unbound/unbound_server.key"
	server-cert-file: "/etc/unbound/unbound_server.pem"
	control-key-file: "/etc/unbound/unbound_control.key"
	control-cert-file: "/etc/unbound/unbound_control.pem"
include: /etc/unbound/conf.d/*.conf
stub-zone:
	name: "s03.as.learn"
	stub-addr: 10.16.255.3
	stub-zone:
		name:		"255.16.10.in-addr.arpa"
		stub-addr:	10.16.255.3
	forward-zone:
		name:	"."
		forward-addr:	142.232.221.253
EOF

}

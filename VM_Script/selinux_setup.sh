#!/bin/bash -
#===============================================================================
#
#          FILE: selinux_setup.sh
#
#         USAGE: .selinux_setup.sh
#
#   DESCRIPTION: Disable SELINUX 
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: John Choi, yno@my.bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/10/2017 12:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Variable declarations
#todo place variable declarations here

function selinux() {
echo "========================================================================="
echo "Disabling selinux"
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config 
}

selinux


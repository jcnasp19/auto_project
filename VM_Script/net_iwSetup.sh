#!bin/bash
declare iw_devInt

function iw_devName () {

echo "========================================================================="
echo "Checking for wireless network card name"

nmcli dev status | grep wifi > /tmp/wifi_name.txt
iw_devInt=$( sed 's/|/ /' /tmp/wifi_name.txt | awk '{print $1}')
}

function iw_confFile () {
echo "========================================================================="
echo "Wifi device configuration file"

cat << _EOF >>  /etc/sysconfig/network-scripts/ifcfg-$iw_devInt

DEVICE=$iw_devInt
BOOTPROTO="none"
TYPE="Wireless"
ONBOOT=yes
NM_CONTROLLED=no
IPADDR=10.16.3.200
PREFIX=25
ESSID="NASP19_3"
CHANNEL="11"
MODE="Master"
RATE="Auto"

_EOF

sed -i "s/adapter/$iw_devInt/" /etc/sysconfig/network-scripts/ifcfg-$iw_devInt

}

function iw_apdSetup () {
echo "========================================================================="
echo "Hostapd configuration"

cat << EOF >> /etc/hostapd/hostapd.conf

ctrl_interface=/var/run/hostapd
ctrl_interface_group=wheel
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
driver=nl80211
interface=adapter
hw_mode=g
channel=11
ssid=NASP19_3

EOF

sed -i "s/adapter/$iw_devInt/" /etc/hostapd/hostapd.conf

systemctl restart network
systemctl start hostapd
systemctl enable hostapd

}
